import React from 'react';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nameValue: undefined,
      emailValue: undefined,
    }
    this.nameInput = React.createRef();
    this.mailInput = React.createRef();
  }

  updateInputValues = (event) => {
    switch (event.target.id) {
      case 'name':
        this.setState({
          nameValue: event.target.value,
        });
        break;
      case 'email':
        this.setState({
          emailValue: event.target.value,
        });
        break;
      default:
        break;
    }
  }

  focusInput = () => {
    if (!this.state.nameValue) {
      this.nameInput.current.focus();
    } else {
      this.mailInput.current.focus();
    }
  }

  render() {
    return (
      <div className="App-header">
        <div>
          Name :
        <input
            id="name"
            type="text"
            onChange={this.updateInputValues}
            ref={this.nameInput} />
        </div>
        <div>
          Email :
        <input
            id="email"
            type="email"
            ref={this.mailInput} />
        </div>
        <div>
          Contact :
        <input
            type="text"
          />
        </div>
        <div>
          Password :
        <input
            type="password"
          />
        </div>
        <div className="App-button">
          <input
            type="button"
            value="Submit"
            onClick={this.focusInput}
          />
        </div>
      </div>
    );
  }
}

export default App;
